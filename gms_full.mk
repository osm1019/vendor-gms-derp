#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# Quick Tap
ifeq ($(TARGET_SUPPORTS_QUICK_TAP), true)
PRODUCT_PACKAGES += \
    quick_tap
endif

# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome \
    Chrome-Stub \
    DevicePolicyPrebuilt \
    Drive \
    GoogleContacts \
    GoogleTTS \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    Maps \
    MarkupGoogle \
    NgaResources \
    Photos \
    PlayAutoInstallConfig \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundAmplifierPrebuilt \
    SoundPickerPrebuilt \
    SwitchAccessPrebuilt \
    TrichromeLibrary \
    TrichromeLibrary-Stub \
    Tycho \
    VoiceAccessPrebuilt \
    VZWAPNLib \
    WebViewGoogle \
    WebViewGoogle-Stub \
    arcore \
    talkback

ifeq ($(TARGET_IS_PIXEL), true)
PRODUCT_PACKAGES += \
    GoogleCamera \
    SCONE
endif

PRODUCT_PACKAGES += \
    DreamlinerPrebuilt \
    DreamlinerUpdater

ifeq ($(TARGET_IS_PIXEL_6), true)
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2021
else ifeq ($(TARGET_IS_PIXEL_7), true)
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2022
else ifeq ($(TARGET_IS_PIXEL_FOLD), true)
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2022 \
    PixelWallpapers2023Foldable
else ifeq ($(TARGET_IS_PIXEL_8), true)
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2023
else
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2020
endif

# product/priv-app
PRODUCT_PACKAGES += \
    AdaptiveVPNPrebuilt \
    AmbientStreaming \
    AndroidAutoStubPrebuilt \
    AppDirectedSMSService \
    BetterBugStub \
    CarrierLocation \
    CarrierMetrics \
    CarrierWifi \
    CbrsNetworkMonitor \
    ConfigUpdater \
    ConnMO \
    DCMO \
    DMService \
    FilesPrebuilt \
    GCS \
    GoogleDialer \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    HealthIntelligenceStubPrebuilt \
    KidsSupervisionStub \
    MaestroPrebuilt \
    MyVerizonServices \
    OdadPrebuilt \
    OemDmTrigger \
    PartnerSetupPrebuilt \
    Phonesky \
    PixelSupportPrebuilt \
    RecorderPrebuilt \
    SafetyHubPrebuilt \
    ScribePrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt \
    Showcase \
    TetheringEntitlement \
    TurboPrebuilt \
    Velvet \
    VzwOmaTrigger \
    WeatherPixelPrebuilt \
    WellbeingPrebuilt \
    WfcActivation

# system/app
PRODUCT_PACKAGES += \
    GoogleExtShared \
    GooglePrintRecommendationService

# system/priv-app
PRODUCT_PACKAGES += \
    DocumentsUIGoogle \
    TagGoogle

# system_ext/app
PRODUCT_PACKAGES += \
    EmergencyInfoGoogleNoUi \
    Flipendo

# system_ext/priv-app
PRODUCT_PACKAGES += \
    CarrierSetup \
    ConnectivityThermalPowerManager \
    GoogleFeedback \
    GoogleServicesFramework \
    PixelSetupWizard \
    QuickAccessWallet \
    RilConfigService \
    StorageManagerGoogle \
    TurboAdapter \
    grilservice

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    AndroidPlatformServices \
    MlkitBarcodeUIPrebuilt \
    VisionBarcodePrebuilt

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni


ifeq ($(TARGET_IS_PIXEL), true)
PRODUCT_PACKAGES += \
    ClearCallingSettingsOverlay2022 \
    ManagedProvisioningPixelOverlay \
    NowPlayingOverlay \
    PixelBatteryHealthOverlay \
    PixelConfigCustomOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizardOverlay2021
endif

ifeq ($(TARGET_IS_PIXEL_7), true)
PRODUCT_PACKAGES += \
    PixelConnectivityOverlay2022
endif

$(call inherit-product, vendor/gms/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/gms/system/blobs/system_blobs.mk)
$(call inherit-product, vendor/gms/system_ext/blobs/system-ext_blobs.mk)
